import sys
from os import path

from sprites import *
from tilemap import *


class Game:
    def __init__(self):
        """
        initialize game window, etc
        """
        pg.init()
        self.screen = pg.display.set_mode((WIDTH, HEIGHT))
        pg.display.set_caption(TITLE)
        self.clock = pg.time.Clock()
        pg.key.set_repeat(500, 100)
        self.all_sprites = None
        self.walls = None
        self.running = True
        self.playing = True
        self.player = None
        self.map = None
        self.load_data()
        self.camera = None

    def load_data(self):
        game_folder = path.dirname(__file__)
        self.map = Map(path.join(game_folder, 'map.txt'))

    def new(self):
        """
        start a new game
        :return:
        """
        self.all_sprites = pg.sprite.Group()
        self.walls = pg.sprite.Group()
        # loading the map
        for y, line in enumerate(self.map.data):
            for x, item in enumerate(line):
                if item == '1':
                    Wall(self, x, y)
                if item == 'P':
                    self.player = Player(self, x, y)
        self.camera = Camera(self.map.width, self.map.height)

    def run(self):
        """
        game loop
        :return:
        """
        self.playing = True
        while self.playing:
            self.dt = self.clock.tick(FPS) / 1000
            self.events()
            self.update()
            self.draw()

    def update(self):
        """
        game loop - update
        :return:
        """
        self.all_sprites.update()
        self.camera.update(self.player)

    def events(self):
        """
        gamme loop - events
        :return:
        """
        for event in pg.event.get():
            # check for closing window
            if event.type == pg.QUIT:
                Game.quit()
            if event.type == pg.KEYDOWN:
                if event.key == pg.K_ESCAPE:
                    Game.quit()

    def draw_grid(self):
        """
        draw the grid on the screen
        :return:
        """
        for x in range(0, WIDTH, TILESIZE):
            pg.draw.line(self.screen, LIGHTGRAY, (x, 0), (x, HEIGHT))
        for y in range(0, HEIGHT, TILESIZE):
            pg.draw.line(self.screen, LIGHTGRAY, (0, y), (WIDTH, y))

    def draw(self):
        """
        game loop - draw
        :return:
        """
        self.screen.fill(BGCOLOR)
        self.draw_grid()
        for sprite in self.all_sprites:
            self.screen.blit(sprite.image, self.camera.apply(sprite))
        # *after* drawing everything, flip the display
        pg.display.flip()

    def show_start_screen(self):
        """
        game start screen
        :return:
        """
        pass

    def show_go_screen(self):
        """
        game over / continue
        :return:
        """
        pass

    @staticmethod
    def quit():
        pg.quit()
        sys.exit()


if __name__ == "__main__":
    g = Game()
    g.show_start_screen()
    while g.running:
        g.new()
        g.run()
        g.show_go_screen()
